--[[
		Minetest-mod "Bricks Plus", Adds different brick patterns
		Copyright (C) 2022 J. A. Anders

		This program is free software; you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation; version 3 of the License.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
		MA 02110-1301, USA.
]]

--
-- Mod Version 0.1
--

bricks_plus = {}

-- Get Translator
local S = minetest.get_translator("bricks_plus")
bricks_plus.get_translator = S
local S = bricks_plus.get_translator
local has_stairsplus = minetest.get_modpath("stairsplus")


-- The chisel
-- Used to change the pattern of the bricks

minetest.register_craftitem("bricks_plus:chisel", {
  description = S("Chisel\nCraft it together with a brick block to change its pattern"),
  inventory_image = "bricks_plus_chisel.png",
})


-- The bricks

nodenames = {"stone", "sandstone", "desert_sandstone", "silver_sandstone", "desert_stone"}
brick_names = {"stonebrick", "sandstonebrick", "desert_sandstone_brick", "silver_sandstone_brick", "desert_stonebrick"}
descriptions = {"Stone Brick", "Sandstone Brick", "Desert Sandstone Brick", "Silver Sandstone Brick", "Desert Stone Brick"}

for i = 1, 5 do
  current_brick = brick_names[i]
  current_node = nodenames[i]
  current_desc = descriptions[i]
  for j = 1, 4 do
    minetest.register_node("bricks_plus:"..current_brick.."_"..j, {
      description = S(current_desc),
      inventory_image = minetest.inventorycube("default_"..current_node..".png^bricks_plus_pattern_"..j..".png"),
      tiles = {"default_"..current_node..".png^bricks_plus_pattern_"..j..".png"},
      groups = {cracky = 2, stone = 1},
    })

    if has_stairsplus then
      stairsplus.api.register_all("bricks_plus:"..current_brick.."_"..j)
    end

    minetest.register_craft({
      type = "shapeless",
      output = "bricks_plus:"..current_brick.."_"..j+1,
      recipe = {
        "bricks_plus:"..current_brick.."_"..j,
        "bricks_plus:chisel",
      },
      replacements = {{"bricks_plus:chisel", "bricks_plus:chisel"}},
    })
  end
  minetest.register_node("bricks_plus:"..current_brick.."_mossy", {
    description = S("Mossy "..current_desc),
    inventory_image = minetest.inventorycube("default_"..current_node.."_brick.png^bricks_plus_moss.png"),
    tiles = {"default_"..current_node.."_brick.png^bricks_plus_moss.png"},
    groups = {cracky = 2, stone = 1},
  })

  if has_stairsplus then
    stairsplus.api.register_all("bricks_plus:"..current_brick.."_mossy")
  end

  minetest.register_abm({
    nodenames = {"default:"..current_brick},
    neighbors = {"default:water_source", "default:water_flowing"},
    interval = 16,
    chance = 200,
    action = function(pos)
      minetest.set_node(pos, {name = "bricks_plus:"..current_brick.."_mossy"})
    end
  })

  minetest.register_craft({
    type = "shapeless",
    output = "bricks_plus:"..current_brick.."_1",
    recipe = {
      "default:"..current_brick,
      "bricks_plus:chisel",
    },
    replacements = {{"bricks_plus:chisel", "bricks_plus:chisel"}},
  })

  minetest.register_craft({
    type = "shapeless",
    output = "default:"..current_brick,
    recipe = {
      "bricks_plus:"..current_brick.."_4",
      "bricks_plus:chisel",
    },
    replacements = {{"bricks_plus:chisel", "bricks_plus:chisel"}},
  })
end

minetest.register_craft({
  type = "shaped",
  output = "bricks_plus:chisel",
  recipe = {
    {"", "default:steel_ingot", ""},
    {"default:stick", "", ""},
  },
})
